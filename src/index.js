import './index.scss'
import Cookies from 'js-cookie'
import moment from 'moment';

if ((Cookies.get('token') == "" || Cookies.get("token") == undefined) && window.location.href.indexOf("index") < 0) {
    window.location.href = "/index.html";
}

document.addEventListener("DOMContentLoaded", function (event) {
    console.log("here")

    if (window.location.href.indexOf("labelling") > 0) {
        if ($(window).width() < 576) {
            $(".stip_container").css("height", "auto")
        }
        let samples = JSON.parse(Cookies.get("samples"))
        console.log(JSON.parse(Cookies.get("samples")))

        $("#stip_labelling-dataset-name").text(Cookies.get("dataset_name"))
        // append samples into slider
        samples.samples.forEach(function (el, i) {
            $("#stip_samples-slider").append(`
                <div class="swiper-slide d-flex justify-content-center">
                    <div class="card text-center shadow stip_card">
                        <div class="card-body">
                            <p class="stip_p" id="stip_samples-txt-` + i + `">` + el + `</p>
                            <div class="stip_label-btn-box-` + i + `">

                            </div>
                        </div>
                    </div>
                </div>
            `)
            samples.buttons.forEach(function (el) {
                $(".stip_label-btn-box-" + i).append(`
                    <button type="button" class="btn stip_btn-secondary stip_label-btn mt-2" value=` + el + ` sample_id=` + i + `>` + el + `</button>
                `)
            })
        })

        // swiper setup
        if ($(window).width() < 576) {
            var mySwiper = new Swiper('.swiper-container', {
                loop: false,
                spaceBetween: 50,
                navigation: {
                    nextEl: '.swiper-button-next',
                    prevEl: '.swiper-button-prev',
                },
            })
        } else {
            var mySwiper = new Swiper('.swiper-container', {
                loop: false,
                spaceBetween: 50,
                touchRatio: 0,
                navigation: {
                    nextEl: '.swiper-button-next',
                    prevEl: '.swiper-button-prev',
                },
            })
        }


        // labelling of the samples
        let labelling = {}
        $(".stip_label-btn").click(function () {
            $(".stip_label-btn[sample_id=" + $(this).attr("sample_id") + "]").removeClass("stip_btn")
            $(".stip_label-btn[sample_id=" + $(this).attr("sample_id") + "]").addClass("stip_btn-secondary")
            $(this).removeClass("stip_btn-secondary")
            $(this).toggleClass("stip_btn")
            mySwiper.slideNext();

            labelling[$("#stip_samples-txt-" + $(this).attr("sample_id")).text()] = $(this).val()
            console.log(labelling)
        })

        // send labelling
        $("#stip_send-labelling-btn").click(function () {
            $.ajax({
                url: "http://dev-be.stipworld.com/api/single_dataset_row/" + Cookies.get("selected_item") + "/",
                type: 'POST',
                crossDomain: true,
                contentType: 'application/json',
                data: JSON.stringify(labelling),
                headers: {
                    "Authorization": "Token " + Cookies.get('token'),
                },
                success: function (res) {
                    console.log(res)
                    $("#stip_labelling-modal-title").text("Labelling completato con successo!")
                    $(".modal-body").hide()
                    $(".modal-footer").hide()
                    setTimeout(function () {
                        $('#stip_labelling-modal').modal('hide')
                        window.location.href = "./label-dataset.html";
                    }, 1200)
                },
                error: function (err) {
                    console.log(err)
                }
            })
        })
    }

    //login ajax call
    $("#stip_login-form").submit(function (e) {
        e.preventDefault()
        let data = {
            "username": $("#stip_username").val(),
            "password": $("#stip_password").val()
        }
        $.ajax({
            url: "http://dev-be.stipworld.com/user/",
            type: 'POST',
            crossDomain: true,
            contentType: 'application/json',
            data: JSON.stringify(data),
            success: function (res) {
                console.log(res)
                Cookies.set('token', res.token)
                $("#stip_login-error").remove()
                window.location.href = "./home.html";
            },
            error: function (err) {
                console.log(err)
                $("#stip_login-error").remove()
                $("#stip_login-form").append(`
                    <p class="stip_label mt-3" id="stip_login-error" style="color: red;">Username o password errate, riprova.</p>
                `)
            }
        })
    })

    // logout
    $("#stip_logout-btn").click(function () {
        Cookies.remove('token')
        window.location.href = "./home.html";
    })

    let selected_item = ""
    if (window.location.href.indexOf("scraping") > 0) {
        // end time plus 30dd placeholder and value
        var date_plus = moment().add(30, 'days').toISOString();
        $("#stip_end-time").attr("placeholder", date_plus)
        $("#stip_end-time").attr("value", date_plus)

        // get for 'scraping' list
        $.ajax({
            url: "http://dev-be.stipworld.com/api/stip_scraping/",
            type: 'GET',
            contentType: 'application/json',
            crossDomain: true,
            headers: {
                "Authorization": "Token " + Cookies.get('token'),
            },
            success: function (res) {
                console.log(res)
                res.forEach(function (el) {
                    $("#stip_scraping-box").append(`
                        <div class="row stip_table-row py-3 align-items-center" id=` + el.id + `>
                            <div class="col-7">
                                <p class="stip_h2 m-0">` + el.page_name + `</p>
                                <p class="stip_p m-0">Status: ` + el.status + `</p>
                            </div>
                            <div class="col-5 d-flex justify-content-end">
                                <button type="button" title="edit" selected_id=` + el.id + `
                                    class="btn p-1 p-md-2 stip_color stip_add-btn material-icons stip_edit-btn" data-toggle="modal"
                                    data-target="#stip_edit-item-modal">
                                    edit
                                </button>
                                <button type="button" title="delete" selected_id=` + el.id + `
                                    class="btn p-1 p-md-2 stip_color stip_add-btn material-icons stip_delete-btn" data-toggle="modal"
                                    data-target="#stip_delete-item-modal">
                                    delete
                                </button>
                            </div>
                        </div>
                    `)
                })

                // get item info
                $(".stip_edit-btn").click(function () {
                    selected_item = $(this).attr("selected_id")
                    console.log(selected_item)
                    $.ajax({
                        url: "http://dev-be.stipworld.com/api/stip_scraping/" + selected_item,
                        type: 'GET',
                        crossDomain: true,
                        contentType: 'application/json',
                        headers: {
                            "Authorization": "Token " + Cookies.get('token'),
                        },
                        success: function (res) {
                            console.log(res)
                            $("#stip_edit-modal-content").empty()
                            $("#stip_edit-modal-content").append(`
                                <form id="stip_new-item-form">
                                    <div class="form-group">
                                        <label for="stip_page-name">Page Name</label>
                                        <input type="text" class="form-control" id="stip_edit-page-name" value=` + res.page_name + ` maxlength="50" required>
                                    </div>
                                    <div class="form-group">
                                        <label for="stip_social-kind">Social Kind</label>
                                        <select class="form-control" id="stip_edit-social-kind" required>
                                            <option value="facebook">Facebook</option>
                                            <option value="twitter">Twitter</option>
                                        </select>
                                    </div>
                                    <div class="form-group">
                                        <label for="stip_interval-time">Interval Time</label>
                                        <input type="number" class="form-control" id="stip_edit-interval-time" value=` + res.interval_time + ` required>
                                        <small class="form-text text-muted stip_label">Inserisci ogni quanto vuoi che lo scraping venga ripetuto in automatico</small>
                                    </div>
                                    <div class="form-group">
                                        <label for="stip_interval-type">Interval Type</label>
                                        <select class="form-control" id="stip_edit-interval-type" required>
                                            <option value="days">Days</option>
                                            <option value="week">Week</option>
                                        </select>
                                    </div>
                                    <div class="form-group">
                                        <label for="stip_end-time">End Time (gg/mm/aaaa)</label>
                                        <input class="form-control" id="stip_edit-end-time" type="date" value=` + moment(res.end_datetime).format("YYYY-MM-DD") + ` required>
                                        <small class="form-text text-muted stip_label">Inserisci la data di quando dovrà terminane lo scraping</small>
                                    </div>
                                    <div class="form-group">
                                        <label for="stip_scrape-until">Scrape Until (gg/mm/aaaa)</label>
                                        <input class="form-control" id="stip_edit-scrape-until" type="date" value=` + moment(res.scrape_until).format("YYYY-MM-DD") + ` required>
                                        <small class="form-text text-muted stip_label">Inserisci la data fino a quando ti servono i dati screpati</small>
                                    </div>
                                    <div class="form-group">
                                        <label for="stip_labels">Labels</label>
                                        <input type="text" class="form-control" id="stip_edit-labels" maxlength="100" value="` + res.labels.join(", ") + `" required>
                                        <small class="form-text text-muted stip_label">Inserisci le etichette separandole con una virgola</small>
                                    </div>
                                </form>
                            `)
                            $("#stip_edit-interval-type").val(res.interval_type)
                            $("#stip_edit-social-kind").val(res.social_kind)
                        },
                        error: function (err) {
                            console.log(err)
                        }
                    })
                })

                $(".stip_delete-btn").click(function () {
                    selected_item = $(this).attr("selected_id")
                })
            },
            error: function (err) {
                console.log(err)
            }
        })

        // create scraping
        $("#stip_new-item-form").submit(function (e) {
            e.preventDefault()

            let data = {
                "page_name": $("#stip_page-name").val(),
                "social_kind": $("#stip_social-kind").val(),
                "interval_time": parseInt($("#stip_interval-time").val()),
                "interval_type": $("#stip_interval-type").val(),
                "end_datetime": $("#stip_end-time").val(),
                "scrape_until": moment($("#stip_scrape-until").val()).toISOString(),
                "labels": $("#stip_labels").val().split(','),
            }
            console.log(data)
            $.ajax({
                url: "http://dev-be.stipworld.com/api/stip_scraping/",
                type: 'POST',
                crossDomain: true,
                contentType: 'application/json',
                headers: {
                    "Authorization": "Token " + Cookies.get('token'),
                },
                data: JSON.stringify(data),
                success: function (res) {
                    console.log(res)
                    $('#stip_new-item-modal').modal('hide')
                    location.reload()
                },
                error: function (err) {
                    console.log(err)
                }
            })
        })
    }

    if (window.location.href.indexOf("dataset") > 0) {
        // get for 'dataset' list
        $.ajax({
            url: "http://dev-be.stipworld.com/api/general_dataset/",
            type: 'GET',
            contentType: 'application/json',
            crossDomain: true,
            headers: {
                "Authorization": "Token " + Cookies.get('token'),
            },
            success: function (res) {
                console.log(res)
                res.forEach(function (el) {
                    $("#stip_dataset-box").append(`
                        <div class="row stip_table-row py-3 align-items-center" id=` + el.id + ` dataset_name="` + el.dataset_name + `">
                            <div class="col-7">
                                <p class="stip_h2 m-0">` + el.dataset_name + `</p>
                                <p class="stip_p m-0">Missing rows: ` + el.missing_rows_to_finish + `</p>
                            </div>
                            <div class="col-5 d-flex justify-content-end">
                                <button type="button" title="labelling" selected_id=` + el.id + `
                                    class="btn p-1 p-md-2 stip_color stip_add-btn material-icons stip_labelling-btn"
                                    data-toggle="modal" data-target="#stip_labelling-modal">
                                    forward
                                </button>
                                <button type="button" title="edit" selected_id=` + el.id + `
                                    class="btn p-1 p-md-2 stip_color stip_add-btn material-icons stip_edit-btn" data-toggle="modal"
                                    data-target="#stip_edit-item-modal">
                                    edit
                                </button>
                                <button type="button" title="delete" selected_id=` + el.id + `
                                    class="btn p-1 p-md-2 stip_color stip_add-btn material-icons stip_delete-btn" data-toggle="modal"
                                    data-target="#stip_delete-item-modal">
                                    delete
                                </button>
                            </div>
                        </div>
                    `)
                })

                // get item info
                $(".stip_edit-btn").click(function () {
                    console.log($("#stip_edit-csv")[0])
                    selected_item = $(this).attr("selected_id")
                    console.log(selected_item)
                    $.ajax({
                        url: "http://dev-be.stipworld.com/api/single_dataset/" + selected_item,
                        type: 'GET',
                        crossDomain: true,
                        contentType: 'application/json',
                        headers: {
                            "Authorization": "Token " + Cookies.get('token'),
                        },
                        success: function (res) {
                            console.log(res)
                            $("#stip_edit-modal-content").empty()
                            $("#stip_edit-modal-content").append(`
                                <form id="stip_new-item-form">
                                    <div class="form-group">
                                        <label for="stip_dataset-name">Dataset Name</label>
                                        <input type="text" class="form-control" id="stip_edit-dataset-name" maxlength="100" value=` + res.dataset_name + ` required>
                                    </div>
                                    <div class="form-group">
                                        <label for="stip_csv">File .csv</label>
                                        <input type="file" class="form-control-file" id="stip_edit-csv" required>
                                    </div>
                                    <div class="form-group">
                                        <label for="stip_dataset-sample-feature">Dataset Sample Feature</label>
                                        <input type="text" class="form-control" id="stip_edit-dataset-sample-feature" maxlength="100" value=` + res.dataset_sample_feature + `
                                            required>
                                    </div>
                                    <div class="form-group">
                                        <label for="stip_dataset-target-title">Dataset Target Title</label>
                                        <input type="text" class="form-control" id="stip_edit-dataset-target-title" placeholder="target"
                                            value="target" maxlength="100" value=` + res.dataset_target_title + ` required>
                                    </div>
                                    <div class="form-group">
                                        <label for="stip_labels">Labels</label>
                                        <input type="text" class="form-control" id="stip_edit-labels" maxlength="100" value="` + res.labels.join(", ") + `" required>
                                        <small class="form-text text-muted stip_label">Inserisci le etichette separandole con una
                                            virgola</small>
                                    </div>
                                </form>
                            `)
                        },
                        error: function (err) {
                            console.log(err)
                        }
                    })
                })

                $(".stip_delete-btn").click(function () {
                    selected_item = $(this).attr("selected_id")
                })

                $(".stip_labelling-btn").click(function () {
                    selected_item = $(this).attr("selected_id")
                    Cookies.set("dataset_name", $("#" + selected_item).attr("dataset_name"))
                })
            },
            error: function (err) {
                console.log(err)
            }
        })

        $("#stip_csv").change(function () {
            console.log($("#stip_csv")[0].files[0])
        })

        // create dataset
        $("#stip_new-item-form").submit(function (e) {
            e.preventDefault()

            let formData = new FormData();
            formData.append('dataset_name', $("#stip_dataset-name").val());
            formData.append('dataset_sample_feature', $("#stip_dataset-sample-feature").val());
            formData.append('dataset_target_title', $("#stip_dataset_target_title").val());
            formData.append('labels', $("#stip_labels").val());
            formData.append('dataset_file', $("#stip_csv")[0].files[0]);
            $.ajax({
                url: "http://dev-be.stipworld.com/api/general_dataset/",
                type: 'POST',
                crossDomain: true,
                contentType: false,
                processData: false,
                headers: {
                    "Authorization": "Token " + Cookies.get('token'),
                },
                data: formData,
                success: function (res) {
                    console.log(res)
                    $('#stip_new-item-modal').modal('hide')
                    location.reload()
                },
                error: function (err) {
                    console.log(err)
                }
            })
        })

        // set number of samples and redirect to labbeling page
        $("#stip_labelling-btn").click(function () {
            if ($("#stip_samples-num").val() > 0) {
                $.ajax({
                    url: "http://dev-be.stipworld.com/api/single_dataset_row/" + selected_item + "/?n_rows=" + $("#stip_samples-num").val(),
                    type: 'GET',
                    crossDomain: true,
                    contentType: 'application/json',
                    headers: {
                        "Authorization": "Token " + Cookies.get('token'),
                    },
                    success: function (res) {
                        console.log(res)
                        Cookies.set("samples", res)
                        Cookies.set("selected_item", selected_item)
                        $("#stip_login-error").remove()
                        $('#stip_labelling-modal').modal('hide')
                        window.location.href = "./labelling.html";
                    },
                    error: function (err) {
                        console.log(err)
                    }
                })
            } else {
                $("#stip_login-error").remove()
                $("#stip_labelling-modal-body").append(`
                    <p class="stip_label mt-3" id="stip_login-error" style="color: red;">Inserisci un numero maggiore di 0</p>
                `)
            }
        })
    }

    // delete an item
    $("#stip_delete-btn").click(function () {
        console.log(selected_item)

        if (window.location.href.indexOf("scraping") > 0) {
            $.ajax({
                url: "http://dev-be.stipworld.com/api/stip_scraping/" + selected_item,
                type: 'DELETE',
                crossDomain: true,
                contentType: 'application/json',
                headers: {
                    "Authorization": "Token " + Cookies.get('token'),
                },
                success: function (res) {
                    console.log(res)
                    $('#stip_delete-item-modal').modal('hide')
                    location.reload()
                },
                error: function (err) {
                    console.log(err)
                }
            })
        } else {
            $.ajax({
                url: "http://dev-be.stipworld.com/api/single_dataset/" + selected_item,
                type: 'DELETE',
                crossDomain: true,
                contentType: 'application/json',
                headers: {
                    "Authorization": "Token " + Cookies.get('token'),
                },
                success: function (res) {
                    console.log(res)
                    $('#stip_delete-item-modal').modal('hide')
                    location.reload()
                },
                error: function (err) {
                    console.log(err)
                }
            })
        }
    })

    // edit an item
    $("#stip_edit-btn").click(function () {
        let data = {}
        if (window.location.href.indexOf("scraping") > 0) {
            data = {
                "page_name": $("#stip_edit-page-name").val(),
                "social_kind": $("#stip_edit-social-kind").val(),
                "interval_time": parseInt($("#stip_edit-interval-time").val()),
                "interval_type": $("#stip_edit-interval-type").val(),
                "end_datetime": moment($("#stip_edit-end-time").val()).toISOString(),
                "scrape_until": moment($("#stip_edit-scrape-until").val()).toISOString(),
                "labels": $("#stip_edit-labels").val().split(','),
            }

            $.ajax({
                url: "http://dev-be.stipworld.com/api/stip_scraping/" + selected_item + "/",
                type: 'PUT',
                crossDomain: true,
                contentType: 'application/json',
                data: JSON.stringify(data),
                headers: {
                    "Authorization": "Token " + Cookies.get('token'),
                },
                success: function (res) {
                    console.log(res)
                    $('#stip_edit-item-modal').modal('hide')
                },
                error: function (err) {
                    console.log(err)
                }
            })
        } else {
            let formData = new FormData();
            formData.append('dataset_name', $("#stip_edit-dataset-name").val());
            formData.append('dataset_sample_feature', $("#stip_edit-dataset-sample-feature").val());
            formData.append('dataset_target_title', $("#stip_edit-dataset-target-title").val());
            formData.append('labels', $("#stip_edit-labels").val());
            if ($("#stip_edit-csv")[0].files[0] != undefined) {
                formData.append('dataset_file', $("#stip_edit-csv")[0].files[0]);
            }

            $.ajax({
                url: "http://dev-be.stipworld.com/api/single_dataset/" + selected_item + "/",
                type: 'PUT',
                crossDomain: true,
                contentType: false,
                processData: false,
                data: formData,
                headers: {
                    "Authorization": "Token " + Cookies.get('token'),
                },
                success: function (res) {
                    console.log(res)
                    $('#stip_edit-item-modal').modal('hide')
                },
                error: function (err) {
                    console.log(err)
                }
            })
        }
    })
})